<?php

declare(strict_types=1);

namespace WindBridges\MessengerEmergencyMode\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Yaml\Yaml;

final class MessengerEmergencyModeExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../../config'));
        $loader->load('services.yaml');

        $defaultConfig = Yaml::parseFile(__DIR__.'/../../config/messenger_emergency_mode.yaml');
        array_unshift($configs, $defaultConfig['messenger_emergency_mode']);

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('messenger_emergency_mode.error_count', $config['error_count']);
        $container->setParameter('messenger_emergency_mode.delay.min', $config['delay']['min']);
        $container->setParameter('messenger_emergency_mode.delay.max', $config['delay']['max']);
        $container->setParameter('messenger_emergency_mode.delay.step', $config['delay']['step']);
        $container->setParameter('messenger_emergency_mode.notification.app_name', $config['notification']['app_name']);
        $container->setParameter('messenger_emergency_mode.enabled', $config['enabled']);
    }

    public function prepend(ContainerBuilder $container): void
    {
        $container->prependExtensionConfig('framework', [
            'cache' => ['pools' => [
                'cache.messenger.emergency_mode' => null,
            ]],
        ]);
    }
}
