<?php

declare(strict_types=1);

namespace WindBridges\MessengerEmergencyMode\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

use function is_int;

final class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('messenger_emergency_mode');

        /** @phpstan-ignore-next-line */
        $root = $treeBuilder
            ->getRootNode()
            ->children()
        ;

        $root
            ->booleanNode('enabled');

        $root
            ->integerNode('error_count')
            ->defaultValue(50)
        ;

        $delayNode = $root->arrayNode('delay');

        $delayNode
            ->beforeNormalization()
            ->ifTrue(fn ($v) => is_int($v))
            ->then(fn ($v) => ['min' => $v, 'max' => $v, 'step' => $v])
        ;

        $delayNode
            ->children()
            ->scalarNode('min')
            ->defaultValue(10)
        ;

        $delayNode
            ->children()
            ->scalarNode('max')
            ->defaultValue(60)
        ;

        $delayNode
            ->children()
            ->scalarNode('step')
            ->defaultValue(5)
        ;

        $notifierNode = $root
            ->arrayNode('notification')
            ->children()
        ;

        $notifierNode
            ->scalarNode('app_name')
            ->cannotBeEmpty()
        ;

        $notifierNode
            ->booleanNode('enabled')
        ;

        return $treeBuilder;
    }
}
