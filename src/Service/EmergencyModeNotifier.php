<?php

declare(strict_types=1);

namespace WindBridges\MessengerEmergencyMode\Service;

use Symfony\Component\Notifier\Bridge\Telegram\TelegramOptions;
use Symfony\Component\Notifier\ChatterInterface;
use Symfony\Component\Notifier\Message\ChatMessage;
use WindBridges\MessengerEmergencyMode\DTO\EmergencyModeState;

final class EmergencyModeNotifier
{
    public function __construct(
        private readonly ?ChatterInterface $chatter,
        private readonly string $appName,
        private readonly int $errorCount,
    ) {
    }

    public function isSupported(): bool
    {
        return (bool)$this->chatter;
    }

    public function notifyOffline(EmergencyModeState $state): void
    {
        if ($this->chatter) {
            $text = $this->composeOfflineMessageText($state);
            $options = new TelegramOptions();
            $options->parseMode('HTML');
            $this->chatter->send(new ChatMessage($text, $options));
        }
    }

    public function notifyOnline(EmergencyModeState $state): void
    {
        if ($this->chatter) {
            $text = $this->composeOnlineMessageText($state);
            $options = new TelegramOptions();
            $options->parseMode('HTML');
            $this->chatter->send(new ChatMessage($text, $options));
        }
    }

    private function composeOfflineMessageText(EmergencyModeState $state): string
    {
        return implode("\n", [
            '⚠️ <b>Application is in emergency mode</b>',
            "<b>{$this->appName}</b> goes to emergency mode due to <b>{$this->errorCount}</b> consecutive errors.",
            '',
            'Last error message:',
            "<code>{$state->getLastError()}</code>",
        ]);
    }

    private function composeOnlineMessageText(EmergencyModeState $state): string
    {
        $downtime = time() - $state->getActivatedAt();

        $formattedDowntime = sprintf(
            '%01dd %01dh %01dm %01ds', $downtime / 86400, $downtime / 3600, $downtime / 60 % 60, $downtime % 60,
        );

        return implode("\n", [
            '✅ <b>Application is online again</b>',
            "<b>{$this->appName}</b> is fixed and fully functional now.",
            '',
            "Downtime was: <b>{$formattedDowntime}</b>",
        ]);
    }
}
