<?php

declare(strict_types=1);

namespace WindBridges\MessengerEmergencyMode\Service;

use WindBridges\MessengerEmergencyMode\DTO\EmergencyModeState;
use Symfony\Component\ErrorHandler\Exception\FlattenException;

final class EmergencyModeService
{
    public function __construct(
        private readonly EmergencyModeCache $cache,
        private readonly EmergencyModeLock $lock,
        private readonly EmergencyModeNotifier $notifier,
        private readonly int $errorCount,
        private readonly int $delayMin,
        private readonly int $delayMax,
        private readonly int $delayStep,
        private readonly bool $enabled,
    ) {
    }

    public function reportError(FlattenException $exception): void
    {
        $this->lock->withUpdateStateLock(function () use ($exception): void {
            $errorCount = $this->cache->increaseErrorCounter();
            $state = $this->cache->getState();

            if (!$state && $errorCount >= $this->errorCount) {
                $state = new EmergencyModeState($this->delayMin, $exception->getMessage());
                $this->cache->setState($state);
                $this->notifier->notifyOffline($state);
            } elseif ($state) {
                if ($state->getCurrentDelay() < $this->delayMax) {
                    $delay = min($state->getCurrentDelay() + $this->delayStep, $this->delayMax);
                    $state->setCurrentDelay($delay);
                }

                $state->refreshUpdatedAt();
                $state->setLastError($exception->getMessage());
                $this->cache->setState($state);
            }
        });
    }

    public function reportSuccess(): void
    {
        $this->lock->withUpdateStateLock(function (): void {
            $state = $this->cache->getState();

            if ($state) {
                $this->cache->resetErrorCounter();
                $this->cache->setState(null);
                $this->notifier->notifyOnline($state);
            }
        });
    }

    public function exclusiveUpdate(callable $update): mixed
    {
        return $this->lock->withExclusiveCheckLock($update(...));
    }

    public function getState(): ?EmergencyModeState
    {
        return $this->lock->withUpdateStateLock(fn () => $this->cache->getState());
    }

    public function canUpdate(EmergencyModeState $state): bool
    {
        return time() - $state->getUpdatedAt() >= $state->getCurrentDelay();
    }

    public function isInEmergencyMode(): bool
    {
        return (bool) $this->getState();
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }
}
