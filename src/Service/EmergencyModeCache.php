<?php

declare(strict_types=1);

namespace WindBridges\MessengerEmergencyMode\Service;

use Psr\Cache\CacheItemPoolInterface;
use WindBridges\MessengerEmergencyMode\DTO\EmergencyModeState;

final class EmergencyModeCache
{
    private const ERROR_COUNTER_KEY = 'errors';
    private const STATE_KEY = 'state';

    public function __construct(
        private readonly CacheItemPoolInterface $cacheMessengerEmergencyMode,
    ) {
    }

    public function increaseErrorCounter(): int
    {
        $item = $this->cacheMessengerEmergencyMode->getItem(self::ERROR_COUNTER_KEY);
        $item->set($item->isHit() ? $item->get() + 1 : 1);
        $this->cacheMessengerEmergencyMode->save($item);

        return $item->get();
    }

    public function resetErrorCounter(): void
    {
        $this->cacheMessengerEmergencyMode->deleteItem(self::ERROR_COUNTER_KEY);
    }

    public function getErrorCounter(): int
    {
        $item = $this->cacheMessengerEmergencyMode->getItem(self::ERROR_COUNTER_KEY);

        return $item->isHit() ? $item->get() : 0;
    }

    public function setState(?EmergencyModeState $state): void
    {
        if (!$state) {
            $this->cacheMessengerEmergencyMode->deleteItem(self::STATE_KEY);

            return;
        }

        $item = $this->cacheMessengerEmergencyMode->getItem(self::STATE_KEY);
        $item->set($state);
        $this->cacheMessengerEmergencyMode->save($item);
    }

    public function getState(): ?EmergencyModeState
    {
        $item = $this->cacheMessengerEmergencyMode->getItem(self::STATE_KEY);

        return $item->isHit() ? $item->get() : null;
    }
}
