<?php

declare(strict_types=1);

namespace WindBridges\MessengerEmergencyMode\Service;

use Symfony\Component\Lock\LockFactory;

final class EmergencyModeLock
{
    private const STATE_LOCK_KEY = 'state-lock';
    private const CHECK_LOCK_KEY = 'check-lock';

    public function __construct(
        private readonly LockFactory $lockFactory,
    ) {
    }

    public function withUpdateStateLock(callable $fn): mixed
    {
        $lock = $this->lockFactory->createLock(self::STATE_LOCK_KEY, 30);
        $lock->acquire(true);

        try {
            return $fn();
        } finally {
            $lock->release();
        }
    }

    public function withExclusiveCheckLock(callable $fn): mixed
    {
        $lock = $this->lockFactory->createLock(self::CHECK_LOCK_KEY);

        if ($lock->acquire()) {
            try {
                return $fn();
            } finally {
                $lock->release();
            }
        }

        // If lock is not acquired, then wait until it is released in other worker
        $lock->acquire(true);
        $lock->release();

        return null;
    }
}
