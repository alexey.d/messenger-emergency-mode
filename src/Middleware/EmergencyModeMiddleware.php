<?php

declare(strict_types=1);

namespace WindBridges\MessengerEmergencyMode\Middleware;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\RecoverableMessageHandlingException;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Stamp\ConsumedByWorkerStamp;
use Throwable;
use WindBridges\MessengerEmergencyMode\Exception\EmergencyModeException;
use WindBridges\MessengerEmergencyMode\Service\EmergencyModeService;

final class EmergencyModeMiddleware implements MiddlewareInterface
{
    public function __construct(
        private readonly EmergencyModeService $service,
    ) {
    }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        if($this->service->isEnabled()) {
            if ($envelope->last(ConsumedByWorkerStamp::class)) {
                $state = $this->service->getState();

                if ($state) {
                    // Emergency mode is active

                    if ($this->service->canUpdate($state)) {
                        $envelope = $this->service->exclusiveUpdate(function () use ($stack, $envelope) {
                            try {
                                return $stack->next()->handle($envelope, $stack);
                            } catch (Throwable $exception) {
                                throw new RecoverableMessageHandlingException($exception->getMessage(), previous: $exception);
                            }
                        });

                        if ($envelope) {
                            return $envelope;
                        }
                    }

                    sleep(1);

                    throw new EmergencyModeException('Delayed due to emergency mode');
                }
            }
        }

        return $stack->next()->handle($envelope, $stack);
    }
}
