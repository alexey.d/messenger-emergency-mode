<?php

declare(strict_types=1);

namespace WindBridges\MessengerEmergencyMode\EventListener;

use RuntimeException;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\Messenger\Event\WorkerMessageFailedEvent;
use Symfony\Component\Messenger\Event\WorkerMessageHandledEvent;
use Symfony\Component\Messenger\Exception\RejectRedeliveredMessageException;
use Symfony\Component\Messenger\Stamp\ErrorDetailsStamp;
use WindBridges\MessengerEmergencyMode\Exception\EmergencyModeException;
use WindBridges\MessengerEmergencyMode\Service\EmergencyModeService;

use function in_array;

final class WorkerEventListener
{
    private static array $ignoredExceptions = [
        RejectRedeliveredMessageException::class,
        EmergencyModeException::class,
    ];

    public function __construct(
        private readonly EmergencyModeService $service,
    ) {
    }

    #[AsEventListener(WorkerMessageHandledEvent::class)]
    public function onMessageHandled(WorkerMessageHandledEvent $event): void
    {
        if($this->service->isEnabled()) {
            $this->service->reportSuccess();
        }
    }

    #[AsEventListener(WorkerMessageFailedEvent::class)]
    public function onMessageFailed(WorkerMessageFailedEvent $event): void
    {
        if($this->service->isEnabled()) {
            $errorDetails = $event->getEnvelope()->last(ErrorDetailsStamp::class);

            if (!$errorDetails || !in_array($errorDetails->getExceptionClass(), self::$ignoredExceptions, true)) {
                $this->service->reportError($errorDetails->getFlattenException());
            }
        }
    }
}
