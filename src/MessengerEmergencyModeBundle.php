<?php

declare(strict_types=1);

namespace WindBridges\MessengerEmergencyMode;

use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use WindBridges\MessengerEmergencyMode\DependencyInjection\MessengerEmergencyModeExtension;

final class MessengerEmergencyModeBundle extends AbstractBundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new MessengerEmergencyModeExtension();
    }
}
