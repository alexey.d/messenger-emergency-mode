<?php

declare(strict_types=1);

namespace WindBridges\MessengerEmergencyMode\DTO;

final class EmergencyModeState
{
    private readonly int $activatedAt;
    private int $updatedAt;

    public function __construct(
        private int $currentDelay,
        private string $lastError,
    ) {
        $this->activatedAt = time();
        $this->updatedAt = time();
    }

    public function getActivatedAt(): int
    {
        return $this->activatedAt;
    }

    public function getUpdatedAt(): int
    {
        return $this->updatedAt;
    }

    public function refreshUpdatedAt(): void
    {
        $this->updatedAt = time();
    }

    public function getCurrentDelay(): int
    {
        return $this->currentDelay;
    }

    public function setCurrentDelay(int $currentDelay): void
    {
        $this->currentDelay = $currentDelay;
    }

    public function getLastError(): string
    {
        return $this->lastError;
    }

    public function setLastError(string $lastError): void
    {
        $this->lastError = $lastError;
    }
}
