<?php

declare(strict_types=1);

namespace WindBridges\MessengerEmergencyMode\Exception;

use Symfony\Component\Messenger\Exception\RecoverableMessageHandlingException;

final class EmergencyModeException extends RecoverableMessageHandlingException
{
}
