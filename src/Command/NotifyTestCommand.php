<?php

declare(strict_types=1);

namespace WindBridges\MessengerEmergencyMode\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WindBridges\MessengerEmergencyMode\DTO\EmergencyModeState;
use WindBridges\MessengerEmergencyMode\Service\EmergencyModeNotifier;

#[AsCommand('emergency:notify:test')]
class NotifyTestCommand extends Command
{
    public function __construct(
        private readonly EmergencyModeNotifier $notifier,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->notifier->isSupported()) {
            $output->writeln('');
            $output->writeln('Notifications are not supported. Run the following command to add support:');
            $output->writeln('  <info>composer require symfony/notifier symfony/telegram-notifier</info>');
            $output->writeln('');

            return Command::FAILURE;
        }

        $this->notifier->notifyOffline(new EmergencyModeState(5, 'Some error'));
        $this->notifier->notifyOnline(new EmergencyModeState(5, 'Some error'));

        return Command::SUCCESS;
    }
}
