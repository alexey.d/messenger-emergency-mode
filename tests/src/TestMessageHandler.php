<?php

namespace WindBridges\MessengerEmergencyModeTest;

use Throwable;

class TestMessageHandler
{
    private ?Throwable $exception = null;

    public function __construct(
        private int $hits = 0
    ) { }

    public function __invoke(TestMessage $message): void
    {
        $this->hits++;

        if ($exception = $this->exception) {
            $this->exception = null;
            throw $exception;
        }
    }

    public function getHits(): int
    {
        return $this->hits;
    }

    public function enqueueException(Throwable $exception): void
    {
        $this->exception = $exception;
    }
}