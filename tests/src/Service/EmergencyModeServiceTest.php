<?php

namespace WindBridges\EmergencyModeTest\Service;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use WindBridges\MessengerEmergencyMode\Service\EmergencyModeService;
use WindBridges\MessengerEmergencyMode\Service\EmergencyModeCache;
use WindBridges\MessengerEmergencyMode\DTO\EmergencyModeState;
use RuntimeException;
use Symfony\Component\ErrorHandler\Exception\FlattenException;

class EmergencyModeServiceTest extends KernelTestCase
{
    public function testEmergencyModeActivation(): void
    {
        /** @var EmergencyModeService $service */
        $service = self::getContainer()->get(EmergencyModeService::class);

        $state = $service->getState();
        self::assertNull($state);

        $errorCount = self::getContainer()->getParameter('messenger_emergency_mode.error_count');

        for ($i = 0; $i < $errorCount; $i++) {
            $service->reportError(FlattenException::createFromThrowable(new RuntimeException('Some error')));
        }

        $state = $service->getState();
        self::assertNotNull($state);
    }

    public function testEmergencyModeDeactivation(): void
    {
        /** @var EmergencyModeCache $cache */
        $cache = self::getContainer()->get(EmergencyModeCache::class);
        $cache->setState(new EmergencyModeState(5, 'Some error'));

        /** @var EmergencyModeService $service */
        $service = self::getContainer()->get(EmergencyModeService::class);

        $state = $service->getState();
        self::assertNotNull($state);

        $service->reportSuccess();
        $state = $service->getState();
        self::assertNull($state);
    }
}