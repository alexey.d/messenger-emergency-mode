<?php

namespace WindBridges\EmergencyModeTest\Service;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use WindBridges\MessengerEmergencyMode\Service\EmergencyModeLock;

class EmergencyModeLockTest extends KernelTestCase
{
    public function testUpdateStateLock(): void
    {
        /** @var EmergencyModeLock $lock */
        $lock = self::getContainer()->get(EmergencyModeLock::class);

        $result = $lock->withUpdateStateLock(fn() => 1);

        self::assertSame(1, $result);
    }

    public function testExclusiveCheckLock(): void
    {
        /** @var EmergencyModeLock $lock */
        $lock = self::getContainer()->get(EmergencyModeLock::class);

        $result = $lock->withExclusiveCheckLock(function () {
            return 1;
        });

        self::assertSame(1, $result);
    }

}