<?php

namespace WindBridges\EmergencyModeTest\Service;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use WindBridges\MessengerEmergencyMode\Service\EmergencyModeCache;
use WindBridges\MessengerEmergencyMode\DTO\EmergencyModeState;

class EmergencyModeCacheTest extends KernelTestCase
{
    public function testIncreaseErrorCounter(): void
    {
        $container = self::getContainer();

        /** @var EmergencyModeCache $cache */
        $cache = $container->get(EmergencyModeCache::class);

        $count = $cache->increaseErrorCounter();

        self::assertSame(1, $count);

        $count = $cache->increaseErrorCounter();

        self::assertSame(2, $count);
    }

    public function testResetErrorCounter(): void
    {
        $container = self::getContainer();

        /** @var EmergencyModeCache $cache */
        $cache = $container->get(EmergencyModeCache::class);

        $count = $cache->increaseErrorCounter();

        self::assertSame(1, $count);

        $cache->resetErrorCounter();

        $count = $cache->increaseErrorCounter();

        self::assertSame(1, $count);
    }

    public function testState(): void
    {
        $container = self::getContainer();

        /** @var EmergencyModeCache $cache */
        $cache = $container->get(EmergencyModeCache::class);

        $state = new EmergencyModeState(5, 'Some error');

        $cache->setState($state);

        $gotState = $cache->getState();

        self::assertNotNull($gotState);
        self::assertSame($state->getActivatedAt(), $gotState->getActivatedAt());
        self::assertSame($state->getUpdatedAt(), $gotState->getUpdatedAt());
        self::assertSame($state->getCurrentDelay(), $gotState->getCurrentDelay());
        self::assertSame($state->getLastError(), $gotState->getLastError());

        $cache->setState(null);
        $gotState = $cache->getState();
        self::assertNull($gotState);

    }
}