<?php

namespace WindBridges\EmergencyModeTest\Middleware;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Messenger\Test\InteractsWithMessenger;
use WindBridges\MessengerEmergencyModeTest\TestMessage;
use WindBridges\MessengerEmergencyModeTest\TestMessageHandler;
use WindBridges\MessengerEmergencyMode\Service\EmergencyModeService;
use WindBridges\MessengerEmergencyMode\Service\EmergencyModeCache;
use RuntimeException;
use WindBridges\MessengerEmergencyMode\DTO\EmergencyModeState;
use WindBridges\MessengerEmergencyMode\Exception\EmergencyModeException;

class EmergencyModeMiddlewareTest extends KernelTestCase
{
    use InteractsWithMessenger;

    public function testSuccessHandling(): void
    {
        $transport = $this->transport();
        $transport->send(new TestMessage());
        $transport->queue()->assertCount(1);
        $transport->process(1);
        $transport->queue()->assertCount(0);

        /** @var TestMessageHandler $handler */
        $handler = self::getContainer()->get(TestMessageHandler::class);
        self::assertSame(1, $handler->getHits());

        /** @var EmergencyModeService $service */
        $service = self::getContainer()->get(EmergencyModeService::class);
        self::assertFalse($service->isInEmergencyMode());
    }

    public function testErrorHandling(): void
    {
        /** @var TestMessageHandler $handler */
        $handler = self::getContainer()->get(TestMessageHandler::class);
        $handler->enqueueException(new RuntimeException('Some error'));

        $transport = $this->transport();
        $transport->send(new TestMessage());
        $transport->queue()->assertCount(1);
        $transport->process(1);
        $transport->queue()->assertCount(0);

        /** @var EmergencyModeCache $cache */
        $cache = self::getContainer()->get(EmergencyModeCache::class);
        self::assertSame(1, $cache->getErrorCounter());
    }

    public function testEmergencyModeActivation(): void
    {
        $errorCount = self::getContainer()->getParameter('messenger_emergency_mode.error_count');

        /** @var TestMessageHandler $handler */
        $handler = self::getContainer()->get(TestMessageHandler::class);

        /** @var EmergencyModeCache $cache */
        $cache = self::getContainer()->get(EmergencyModeCache::class);

        /** @var EmergencyModeService $service */
        $service = self::getContainer()->get(EmergencyModeService::class);

        $transport = $this->transport();

        for ($i = 0; $i < $errorCount; $i++) {
            self::assertFalse($service->isInEmergencyMode());

            $handler->enqueueException(new RuntimeException('Some error'));
            $transport->send(new TestMessage());
            $transport->process(1);

            self::assertSame($i + 1, $cache->getErrorCounter());
        }

        self::assertTrue($service->isInEmergencyMode());
    }

    public function testEmergencyModeException(): void
    {
        /** @var EmergencyModeCache $cache */
        $cache = self::getContainer()->get(EmergencyModeCache::class);

        $cache->setState(new EmergencyModeState(5, 'Some error'));

        /** @var TestMessageHandler $handler */
        $handler = self::getContainer()->get(TestMessageHandler::class);
        $handler->enqueueException(new RuntimeException('Some error'));

        $this->expectException(EmergencyModeException::class);

        $transport = $this->transport();
        $transport->throwExceptions();
        $transport->send(new TestMessage());
        $transport->process(1);
    }

    public function testEmergencyModeDeactivation(): void
    {
        /** @var EmergencyModeCache $cache */
        $cache = self::getContainer()->get(EmergencyModeCache::class);

        $cache->setState(new EmergencyModeState(0, 'Some error'));

        $transport = $this->transport();
        $transport->throwExceptions();
        $transport->send(new TestMessage());
        $transport->process(1);

        /** @var EmergencyModeService $service */
        $service = self::getContainer()->get(EmergencyModeService::class);
        self::assertFalse($service->isInEmergencyMode());
    }
}