<?php

namespace WindBridges\MessengerEmergencyModeTest;

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use WindBridges\MessengerEmergencyMode\MessengerEmergencyModeBundle;
use Zenstruck\Messenger\Test\ZenstruckMessengerTestBundle;

class AppKernel extends Kernel
{
    /**
     * @return array
     */
    public function registerBundles(): iterable
    {
        return [
            new FrameworkBundle(),
            new MessengerEmergencyModeBundle(),
            new ZenstruckMessengerTestBundle(),
        ];
    }

    /**
     * @param LoaderInterface $loader
     */
    public function registerContainerConfiguration(LoaderInterface $loader): void
    {
        $loader->load(__DIR__ . '/../config/services.yaml');
    }
}
