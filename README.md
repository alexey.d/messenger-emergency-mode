## How it works
Bundle is designed for applications with Symfony Messenger that depend on external resources such as sites or services, which can go down periodically. If site is down for some time, all messages can just go to the failed transport after some retries. 

This bundle automatically listens for messenger events. When the result of message handling is an error, it increases the internal error counter. If consequent errors count is bigger than specified limit, the emergency mode is activated.

In emergency mode, when message is received from queue, it returned back after 1 second with RecoverableMessageHandlingException. It guarantees that message's retry counter is not increased and message will not go to the failed transport. Periodically one message is selected to test availability of the external resource. If it's ok, then application exits the emergency mode and normal execution is resumed. 

The bundle uses middleware and some event listeners to achieve this. At any time it can be totally disabled in configuration without removing. 

Additionally, you can configure a telegram channel (for this moment only telegram) to receive the notifications about entering and exiting the emergency mode. 

## Installation

Add repository to composer.json:
```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/alexey.d/messenger-emergency-mode"
    }
  ]
}
```

Then install:
```shell
composer require windbridges/messenger-emergency-mode:dev-main
```
or specific version:
```shell
composer require windbridges/messenger-emergency-mode:0.0.1.x-dev
```

## Setup
- Add middleware to your `config/packages/messenger.yaml`:

```yaml
# config/packages/messenger.yaml

framework:
  messenger:
    buses:
      messenger.bus.default:
        middleware:
          - WindBridges\MessengerEmergencyMode\Middleware\EmergencyModeMiddleware
```

- Add configuration for bundle in `config/packages/messenger_emergency_mode.yaml`:

```yaml
messenger_emergency_mode:
  enabled: true     # Set false to totally disable the emergency mode behavior
  error_count: 50   # Number of consecutive errors to stop message handling and enter into emergency node
  delay:            # Settings of delay between attempts to handle a message when in emergency mode
    min: 10         # Start delay time in seconds
    max: 60         # The delay will gradually increase until it reaches this value (in seconds)
    step: 10        # Step (in seconds) to increase the delay
```

Delay can be shorthanded:
```yaml
messenger_emergency_mode:
  delay: 10         # This instructs to set all properties (min, max, step) to 10
```

The full available configuration you can find in `config/messenger_emergency_mode.yaml` in the bundle directory.

# Configure notifications
- Install symfony notifier for notification support:
```shell
composer require symfony/notifier symfony/telegram-notifier
```
- Add application name for notifications:
```yaml
messenger_emergency_mode:
  notification:
    app_name: Your application
```
- Create a bot using @BotFather or use existing bot. Get its token.
- Create a channel or use existing. Add your bot to that channel.
- Get channel's `chat_id`. To do this, you can send some message to the channel, and then forward it to @JsonDumpBot. Look at `message.forward_from_chat.id` property.
- Put this in `.env` file, replacing `BOT_TOKEN` and `CHAT_ID` to your values: `TELEGRAM_DSN=telegram://BOT_TOKEN@default?channel=CHAT_ID`
- Test it with `php bin/console emergency:notify:test` command

## Clearing cache
To clear emergency mode cache, you should run the following command:
```shell
php bin/console cache:pool:clear cache.messenger.emergency_mode
```
This works because the bundle transparently registers the `cache.messenger.emergency_mode` cache pool and saves all its data there. 

## Run tests
```shell
docker compose run php make test
```
